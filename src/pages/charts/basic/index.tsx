import { FC } from 'react'
import { Card } from 'antd'
import View from '@/components/View'
import ReactEcharts from 'echarts-for-react'

const option = {
  xAxis: {
    type: 'category',
    data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
  },
  yAxis: {
    type: 'value',
  },
  series: [
    {
      data: [120, 200, 150, 80, 70, 110, 130],
      type: 'bar',
    },
  ],
}

const Basic: FC = () => {
  return (
    <View className="app-charts">
      <Card type="inner" title="图表">
        <ReactEcharts option={option} style={{ height: 700 }} />
      </Card>
    </View>
  )
}

export default Basic

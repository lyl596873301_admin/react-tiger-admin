import { FC, useRef, useState } from 'react'
import CanvasDraw from 'react-canvas-draw'

const DrawPage: FC = () => {
  const canvasRef = useRef(null)
  const [brushColor] = useState('#444')
  const [brushRadius] = useState(5)
  const canvasWidth = window.innerWidth - 240
  const canvasHeight = window.innerHeight - 150

  return (
    <CanvasDraw
      ref={canvasRef}
      brushColor={brushColor}
      brushRadius={brushRadius}
      lazyRadius={5}
      canvasWidth={canvasWidth}
      canvasHeight={canvasHeight}
    />
  )
}

export default DrawPage

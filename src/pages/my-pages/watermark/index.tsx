import { FC, useEffect } from 'react'
import { Card } from 'antd'
import View from '@/components/View'
import Watermark from 'watermark-plus'
import { ProjectName } from '@/config/constants'

const Water: FC = () => {
  const watermark = new Watermark({
    content: ProjectName,
  })

  useEffect(() => {
    watermark.create()

    return () => {
      watermark.destroy()
    }
  }, [])
  return (
    <View className="app-guide">
      <Card type="inner" title="开发文档">
        <p>开发文档正在紧张编写中...</p>
      </Card>
    </View>
  )
}

export default Water

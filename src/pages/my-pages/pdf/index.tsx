import { FC, useEffect, useState } from 'react'
import { Document, Page, pdfjs } from 'react-pdf'
// import View from '@/components/View'
import { Card, Pagination, Space } from 'antd'
import './index.less'
import { PageConfig } from '@/config/constants'

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`

const PDF: FC = () => {
  const [page, setPage] = useState(1)
  const [totalPage, setTotalPage] = useState(0)
  const file = `${process.env.VITE_BASE_URL}/pdf/plans.pdf`

  useEffect(() => {
    setPage(1)
  }, [])

  const onDocumentLoadSuccess = ({ numPages }: any) => {
    setTotalPage(numPages)
  }

  const handelOnChange = (pages: any) => {
    setPage(pages)
  }

  return (
    <view className="app-page-pdf">
      <Card type="inner" title="PDF预览">
        <div className="app-pdf flex justify-center">
          <Space direction="vertical" size="middle">
            <Pagination className="pdf_page" {...PageConfig.options} showSizeChanger={false} onChange={handelOnChange} total={totalPage * 10} current={page} />
            <Document file={file} onLoadSuccess={onDocumentLoadSuccess}>
              <Page pageNumber={page} />
            </Document>
          </Space>
        </div>
      </Card>
    </view>
  )
}

export default PDF

import { FC } from 'react'
import * as Icons from '@ant-design/icons'
import Icon from '@ant-design/icons'
import { Col, Row, Space } from 'antd'

const AntIcon: FC = () => {
  const iconList = Object.keys(Icons).filter((item: any) => typeof (Icons[item as keyof typeof Icons]) === 'object' && item.endsWith('Outlined'))

  return (
    <div className="app-ant-icon">
      <Row gutter={[20, 20]}>
        {iconList.map((type: any, index: number) => {
          return (
            <Col key={index} span={3}>
              <Space direction="vertical">
                <Icon component={Icons[type as keyof typeof Icons] as any} />
                {type}
              </Space>
            </Col>
          )
        })}
      </Row>
    </div>
  )
}

export default AntIcon

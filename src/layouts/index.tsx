import React from 'react'
import { Layout } from 'antd'
import AppSider from './Sider'
import AppHeader from './Header'
import AppContent from './Content'
import TagsView from './TagsView'

import './index.less'

const AppLayout = () => {
  return (
    <Layout>
      <AppSider />
      <Layout style={{ minHeight: '100vh' }}>
        <AppHeader />
        <TagsView />
        <AppContent />
      </Layout>
    </Layout>
  )
}

export default AppLayout

import { createContext, useContext } from 'react'
import appStore from './modules/app'
import userStore from './modules/user'
import tagStore from './modules/tag'

const store = {
  appStore,
  userStore,
  tagStore,
}

const StoreContext = createContext(store)

export const useStore = () => useContext(StoreContext)

export default store

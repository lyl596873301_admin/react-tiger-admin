import { IRoute } from '@/typings/router'
import { Navigate, useRoutes } from 'react-router-dom'
import Login from '@/pages/login/index'

// 导入所有router
const metaRouters: any = import.meta.glob('./modules/*.tsx', { eager: true, import: 'default' })

// 处理路由
export const routerArray: IRoute[] = []
Object.keys(metaRouters).forEach((key) => {
  const mod = metaRouters[key] || {}
  const modList = Array.isArray(mod) ? [...mod] : mod
  routerArray.push(...modList)
})

export const rootRouter: Array<any> = [
  {
    path: '/',
    key: '/',
    element: <Navigate to="/login" />,
    hidden: true,
  },
  {
    path: '/login',
    key: '/login',
    element: <Login />,
    hidden: true,
    meta: {
      requiresAuth: false,
      title: '登录页',
      key: 'login',
    },
  },
  ...routerArray,
  {
    path: '*',
    element: <Navigate to="/basic/error/404" />,
  },
]

const Router = () => {
  return useRoutes(rootRouter)
}

export default Router
